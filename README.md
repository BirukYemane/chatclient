# simple chat client 

This project is a chat client app for Leadin chat server developed using angular JS. It was developed based on 
the Angular seed boiler plate which can be found here : https://github.com/angular/angular-seed. 


# Functionalities

Users join the chat by registering a nick name. Up on a successful nick registraton a user's status will be "online" and  
can send and recieve message to/from other users who are also currently online. The nick name is not permannet
it is for the current chat session. When the user joins another time he can pick any nick name that is 
not taken by other users currently.The app show the online status of users.


## Getting Started

To get you started you can simply clone the ChatClient repository 

### Prerequisites

You need git to clone the angular-seed repository. You can get git from
[http://git-scm.com/](http://git-scm.com/).

We also use a number of node.js tools to initialize and test angular-seed. You must have node.js and
its package manager (npm) installed.  You can get them from [http://nodejs.org/](http://nodejs.org/).

Since this chat client communicates with the chat server application, the chat server app must be running. 
The chat server can be hosted either on the localhost or on some remote web server

### Clone ChatClient

Clone the ChatClient repository using git more instructions are available here https://confluence.atlassian.com/bitbucket/clone-a-repository-223217891.html:

```
git clone https://BirukYemane@bitbucket.org/BirukYemane/chatclient.git

```

### Install Dependencies

We have two kinds of dependencies in this project: tools and angular framework code.  The tools help
us manage and test the application.

* We get the tools we depend upon via `npm`, the [node package manager][npm].
* We get the angular code via `bower`, a [client-side code package manager][bower].

You dont need to intall them, they will be automatically installed while running the application. check next section. 


### Run the Application

We have preconfigured the project with a simple development web server.  The simplest way to start
this server is:

```
npm start
```

Now browse to the app at `http://localhost:8000/index.html`.