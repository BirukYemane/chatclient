'use strict';

angular.module('myApp.view1', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view1', {
    templateUrl: 'view1/view1.html',
    controller: 'View1Ctrl'
  });
}])



.controller('View1Ctrl', [ '$scope', '$route', '$http', function($scope,$route,$http) {

		// The variables in the starting() function are to be initialized only when the controller is initialized. 
		// if we want to make them accessable from other views then we have to put them in service 
		// however for simplicity sake only one view is used 

	  var url = "ws://127.0.0.1:8888/";
	  var socket = '';
	    
	  starting(); // initializing, create socket object and other variables to store data
	    
     
	  function starting(){
	      socket = new WebSocket(url);	  	  
		  $scope.chatHistory = []; // stores feched chat history , messages sent by user, and messages recieved. it was thought its better to put them all in one place.
		  $scope.users = []; // stores users 
		  $scope.nickName = ''; // may be not needed if we use required field
		  $scope.message = ''; // may be not needed if we use required field		  
	  }; 

	  
       socket.onmessage = function (evt) {
          // here we check what type of message we get and decide what to do
         	             
	         if (evt.data == "Hello from Leadin chat. Set the nickname with the command /nick nickname before chatting away.") { // successful connection
	         	// show registration UI
	         	$scope.status = 'Hello from Leadin chat. Enter nickname before chatting away.';
          		$scope.showRegister = true; 
         		$scope.$apply(); 	         	          	   

	         }else{ // if it is a message in the format {from:x, message:y}
					var msgJson = angular.fromJson(evt.data); // convert message to JSON object using a local variable
					
					if('error' in msgJson){ // error message
						if(msgJson.error == 'Nick in use'){
							$scope.status = 'Nick in use, pleae enter a nick name again';
							$scope.nickName = '';  
							$scope.$apply();							// 
						}
					}else if (msgJson.from == '_server' && msgJson.message == 'New user: ' + $scope.nickName + ' joined.'){	// successul nick registration	message			
						// get previous chat history and users 
						fetchChatHistory(); 
						fetchUsers();
						// store user joined message recieved(in the form of {from:x, message:y}) in chat history format ({from:x, msg:y},  omitting "timestamp"attribute)					
						var msg = {}; 
						msg.from = msgJson.from;
			  			msg.msg = msgJson.message;
			  			$scope.chatHistory.push(msg); 
			  			// set up the chat UI 
		 	   		    $scope.status = 'Welcome ' + $scope.nickName + ' Enjoy your chat!!';
		 	  		    $scope.showChatAndUser = true; // display chat and user
		 	  		    $scope.showChat = true;
		 	  		    $scope.showRegister = false;
		 	  		    $scope.$apply();
					}else { // recieve other messages from users and from server 
						// store user joined message recieved(in the form of {from:x, message:y}) in chat history format ({from:x, msg:y},  omitting "timestamp"attribute)					
						var msg = {}; 
						msg.from = msgJson.from;
			  			msg.msg = msgJson.message;
			  			$scope.chatHistory.push(msg);
						// check if message is about a user joined or dropped out, to update users data by fetching again 
						if(msgJson.from == '_server' ){  // a simple asumption that a user is joining or dropping out. can be better defined. 
							fetchUsers();
						}
						$scope.$apply();
					}					
	                
	         } 
        };

      socket.onopen = function () {

                         
      };

     
      socket.onerror = function (evt) {
      		// set up the UI to restart all over again, displays error message and a reconnet button
      		$scope.status = 'Not connected to the chat server click the button to try again'; 
      		$scope.showConnect = true; // show the reconnect button to try connectin again
      		$scope.showChatAndUser = false;  
		 	$scope.showRegister = false;
      		$scope.$apply();        		      		
      };

      
	  $scope.reloadRoute = function() {
	     // workes together with the "onerror" event handler, this is triggered when the reconnect button is clicked, 
	     // to reconnect if there was a connection error by reinitializing the controller 
	      $route.reload(); 
	  };

      function validateNick() {
      	// validation for nick name
      		if($scope.nickName == ''){
      	  		 window.alert("Error : Please enter nick name");  
      	  		 return false;    	  		 
      	    } else if ($scope.nickName.substring(0,1) == '_') {      	  		 	
	      	  	window.alert("Error : Nick name cannot start with '_' "); 
		      	$scope.nickName = ''; 
		      	return false;
	      	}else if ($scope.nickName.indexOf(' ') != -1){	      		
	      		window.alert("Nick name cannot contain space");
      	  		$scope.nickName = '';    		
	      		return false;
	      	}else {
	      		return true;
	      	}		        
	  };


      $scope.RegisterNick = function() {
      	  	  
      	  var correctNick = validateNick();
      	  if(correctNick){	      	  	   	  		
	      	socket.send('/nick ' + $scope.nickName); 	      					   		      	  			 		
      	  }		      	  		       	  
      };


      $scope.sendMessage = function() {
             
	        if($scope.message == ''){
	        	window.alert("Error : Please enter message");	
	        } else if ($scope.message.substring(0,1) == '/') {
	        	window.alert("Error : A message cannot start with /");	        	
	        } else {	        		 
				 socket.send($scope.message);   
				// add sent message to chat history (in the format {from:x, msg:y},  omitting "timestamp"attribute
			 	 var msg = {}; 
	   			 msg.from = $scope.nickName;
	  			 msg.msg = $scope.message;
	  			 $scope.chatHistory.push(msg);  
	   			 $scope.message = ''; // clear the text box				 
			}       
      };

      $scope.changeView = function(param){
      	if (param == 'users'){
      		$scope.showUsers = true;
  			$scope.showChat = false;   
      	}else{
      		$scope.showUsers = false;
  			$scope.showChat = true;   
      	}
  		 
      };
  
	 function fetchChatHistory(){
	 	// fetch chat history
	  $http.get("http://127.0.0.1:8888/history")
	  .then(function(response){
	  	 var resp = response.data;
	  	 $scope.chatHistory = resp.data;
	  	}); 

	 };

	  function fetchUsers(){
	   // fetch users
	  	$http.get("http://127.0.0.1:8888/users")
	  .then(function(response){
	  	 var resp = response.data;
	  	 $scope.users = resp.data;	  	  
	  	});  

	 };
	
}]);


